var fetchResult = null;
var brandsList = null;
var template = document.querySelector('#carCard');
var selectedCarOption = "All";

function filterCarData() {
    var cars = fetchResult.cars;
    var selectedCarOption = document.getElementById('brands').value
    var filteredCars = selectedCarOption == "All" ?
                       cars :
                       cars.filter(car => car.brand == selectedCarOption);
    document.getElementById("carsHolder").innerHTML = "";

    filteredCars.forEach(function(currentValue){
        var cardHtml = "<div class='car-card'><div class='header'><h3>" +
                       currentValue.brand +" " + currentValue.model + "</h3></div>" +
                       "<div class='features'><p class='year'>" +
                       currentValue.year +
                       "</p><p class='feature'>" +
                       currentValue.km +
                        "</p></div><input type='button' value='Buy' /></div>";

        document.getElementById("carsHolder").innerHTML += cardHtml;
    });
}

(function() {
    fetch('http://localhost:4502/bin/cardata?allCars=all')
        .then(res => res.json())
        .then((out) => {
            fetchResult = out;
            brandsList = fetchResult.filters["brands"];

            var brandSelect = document.getElementById('brands');
            brandsList.forEach(function(currentValue){
                brandSelect.options[brandSelect.options.length] = new Option(currentValue, currentValue);
            });

            filterCarData();
        });
})();