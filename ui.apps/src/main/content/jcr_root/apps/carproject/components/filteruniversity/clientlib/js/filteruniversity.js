var fetchResult = null;
var countryList = null;
var template = document.querySelector('#universityCard');
var selectedUniversityOption = "All";

function filterUniversityData() {
    var university = fetchResult.university;
    var selectedUniversityOption = document.getElementById('country').value;


    document.getElementById("universityHolder").innerHTML = "";

    filtereduniversity.forEach(function(currentValue){
        var cardHtml = "<div class='university-card'><div class='header'><h3>" +
                       currentValue.country +" " + currentValue.name + "</h3></div>" +
                       "<div class='features'><p class='stateProvince'>stateProvince: " +
                       currentValue.stateProvince +
                       "<div class='features'><p class='webPage'>webPage: " +
                       currentValue.webPage +

                        "</p></div><input type='button' value='Click Here' /></div>";

        document.getElementById("universityHolder").innerHTML += cardHtml;
    });
}

(function() {
    // Call to Servlet
    fetch('http://localhost:4502/bin/universitydata?allUniversity=all')
        .then(res => {
            res.json();
        })
        .then((out) => {
            fetchResult = out;

            //fill the dropdown/select list
            countryList = fetchResult.filters["country"];
            var countrySelect = document.getElementById('country');
            countryList.forEach(function(currentValue){
                countrySelect.options[countrySelect.options.length] = new Option(currentValue, currentValue);
            });

            filterUniversityData();
        });
})();