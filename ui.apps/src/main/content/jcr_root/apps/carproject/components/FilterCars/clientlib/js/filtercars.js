var fetchResult = null;
var brandsList = null;
var template = document.querySelector('#carCard');
var selectedCarOption = "All";


//List the cars depending on the selection
function filterCarData() {
    var cars = fetchResult.cars;
    var selectedCarOption = document.getElementById('brands').value;
    var kmFrom = parseInt(document.getElementById('kmFrom').value);
    var kmTo = parseInt(document.getElementById('kmTo').value);
console.log("hi", cars);
    var filteredCars = selectedCarOption == "All" ?
    cars.filter(car => (!isNaN(kmFrom) ? parseInt(car.km) >= kmFrom : true) &&
    (!isNaN(kmTo) ? parseInt(car.km) <= kmTo : true)):
                       cars.filter(car => car.brand == selectedCarOption &&
                       (!isNaN(kmFrom) ? parseInt(car.km) >= kmFrom : true) &&
                       (!isNaN(kmTo) ? parseInt(car.km) <= kmTo : true));
    document.getElementById("carsHolder").innerHTML = "";

    filteredCars.forEach(function(currentValue){
        var cardHtml = "<div class='car-card'><div class='header'><h3>" +
                       currentValue.brand +" " + currentValue.model + "</h3></div>" +
                       "<div class='features'><p class='year'>year: " +
                       currentValue.year +
                       "</p><p class='feature'>km: " +
                       currentValue.km +
                        "</p></div><input type='button' value='Buy' /></div>";

        document.getElementById("carsHolder").innerHTML += cardHtml;
    });
}

(function() {
    // Call to Servlet
    fetch('http://localhost:4502/bin/cardata?allCars=all')
        .then(res => {
            res.json();
        })
        .then((out) => {
            fetchResult = out;

            //fill the dropdown/select list
            brandsList = fetchResult.filters["brands"];
            var brandSelect = document.getElementById('brands');
            brandsList.forEach(function(currentValue){
                brandSelect.options[brandSelect.options.length] = new Option(currentValue, currentValue);
            });

            filterCarData();
        });
})();