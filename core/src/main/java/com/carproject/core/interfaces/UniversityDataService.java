package com.carproject.core.interfaces;

import com.carproject.core.models.UniversityModel;
import com.carproject.core.models.UniversityResults;
import com.carproject.core.models.Filter;
import org.apache.sling.api.resource.Resource;

import java.util.List;

public interface UniversityDataService {

    UniversityResults filterUniversity(Resource allUniversity, String name, String country, String stateProvince, String webPage );

    Filter getDistinctFilters(List<UniversityModel> results);

}
