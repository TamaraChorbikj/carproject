package com.carproject.core.interfaces;

import com.carproject.core.models.CarModel;
import com.carproject.core.models.CarResults;
import com.carproject.core.models.FilterCars;
import org.apache.sling.api.resource.Resource;

import java.util.List;

public interface CarDataService {

    CarResults filterCars(Resource allCars, String brand, String model, String year);

    FilterCars getDistinctFilters(List<CarModel> results);

}

