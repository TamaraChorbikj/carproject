package com.carproject.core.interfaces;

import com.carproject.core.models.University;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;

import java.util.List;

public interface UniversityDataImportService {

    List<University> importUniversity(Resource resources) throws PersistenceException;
}

