package com.carproject.core.interfaces;

import com.carproject.core.models.Manufacturer;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;

import java.util.List;

public interface CarDataImportService {

    List<Manufacturer> importManufacturers(Resource resources) throws PersistenceException;
}




