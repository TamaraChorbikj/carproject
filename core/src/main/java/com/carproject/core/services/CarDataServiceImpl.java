package com.carproject.core.services;

import com.carproject.core.interfaces.CarDataService;
import com.carproject.core.models.CarModel;
import com.carproject.core.models.CarResults;
import com.carproject.core.models.FilterCars;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.osgi.service.component.annotations.Component;

import java.util.List;

@Component(service = CarDataService.class)
public class CarDataServiceImpl implements CarDataService {
    @Override
    public CarResults filterCars(Resource allCars, String brand, String model, String year) {
        CarResults filteredCars = new CarResults();

        allCars.getChildren().forEach((car) -> {
            CarModel carModel = car.adaptTo((CarModel.class));

            if ((brand == null || brand.equals(carModel.getBrand())) &&
                    (model == null || model.equals(carModel.getModel())) &&
                    (year == null || year.equals(carModel.getYear()))) {
                filteredCars.getCars().add(carModel);
            }

            ModifiableValueMap properties = car.adaptTo(ModifiableValueMap.class);
            String info = carModel.getBrand() + " " + carModel.getModel() + " " + carModel.getYear();
            properties.put("info", info);
            try {
                car.getResourceResolver().commit();
            } catch (PersistenceException e) {
                throw new RuntimeException(e);
            }

        });


        return filteredCars;
    }

    @Override
    public FilterCars getDistinctFilters(List<CarModel> results) {
        FilterCars filters = new FilterCars();

        results.forEach((carModel) -> {

            if (!filters.getBrands().contains(carModel.getBrand())) {
                filters.getBrands().add(carModel.getBrand());
            }

            if (!filters.getModel().contains(carModel.getModel())) {
                filters.getModel().add(carModel.getModel());
            }

            if (!filters.getYear().contains(carModel.getYear())) {
                filters.getYear().add(carModel.getYear());
            }
        });

        return filters;
    }
}
