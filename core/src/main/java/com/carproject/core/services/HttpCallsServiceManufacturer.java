package com.carproject.core.services;

import com.carproject.core.models.ImportedManufacturers;
import com.carproject.core.models.Manufacturer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.osgi.service.component.annotations.Component;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

@Component (service = HttpCallsServiceManufacturer.class)
public class HttpCallsServiceManufacturer {
    List<Manufacturer> importManufacturerFromApi(){
        ObjectMapper mapper = new ObjectMapper();
        ImportedManufacturers importedManufacturers = null;
        try {
            URL url = new URL("https://vpic.nhtsa.dot.gov/api/vehicles/getallmakes?format=json");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();

            if (conn.getResponseCode() == 200) {
                importedManufacturers = mapper.readValue(conn.getInputStream(), ImportedManufacturers.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return importedManufacturers.getResults();
    }
}
