package com.carproject.core.services;

import com.carproject.core.interfaces.UniversityDataService;
import com.carproject.core.models.UniversityModel;
import com.carproject.core.models.UniversityResults;
import com.carproject.core.models.Filter;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.osgi.service.component.annotations.Component;

import java.util.List;

@Component(service = UniversityDataService.class)
public class UniversityDataServiceImpl implements UniversityDataService {
    @Override
    public UniversityResults filterUniversity(Resource allUniversity, String name, String country, String stateProvince, String webPage) {
        UniversityResults filteredUniversity = new UniversityResults();

        allUniversity.getChildren().forEach((university) -> {
            UniversityModel universityModel = university.adaptTo((UniversityModel.class));

            if ((name == null || name.equals(universityModel.getName())) &&
                    (country == null || country.equals(universityModel.getCountry())) &&
                    (stateProvince == null || stateProvince.equals(universityModel.getStateProvince())) &&
                    (webPage == null || webPage.equals(universityModel.getWebPage()))) {
                filteredUniversity.getUniversity().add(universityModel);
            }

            ModifiableValueMap properties = university.adaptTo(ModifiableValueMap.class);
            String info = universityModel.getName() + " " + universityModel.getCountry() + " " + universityModel.getStateProvince() + " " + universityModel.getWebPage();
            properties.put("info", info);
            try {
                university.getResourceResolver().commit();
            } catch (PersistenceException e) {
                throw new RuntimeException(e);
            }

        });

        return filteredUniversity;
    }

    @Override
    public Filter getDistinctFilters(List<UniversityModel> results) {
        Filter filters = new Filter();

        results.forEach((universityModel) -> {

            if (!filters.getName().contains(universityModel.getName())) {
                filters.getName().add(universityModel.getName());
            }

            if (!filters.getCountry().contains(universityModel.getCountry())) {
                filters.getCountry().add(universityModel.getCountry());
            }

            if (!filters.getStateProvince().contains(universityModel.getStateProvince())) {
                filters.getStateProvince().add(universityModel.getStateProvince());
            }

            if (!filters.getWebPage().contains(universityModel.getWebPage())) {
                filters.getWebPage().add(universityModel.getWebPage());
            }
        });

        return filters;
    }
}
