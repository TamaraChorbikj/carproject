package com.carproject.core.services;

import com.carproject.core.interfaces.CarDataImportService;
import com.carproject.core.models.Manufacturer;
import com.google.common.collect.ImmutableMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.List;

@Component(service = CarDataImportService.class)
public class CarDataImportServiceImpl implements CarDataImportService {
    @Reference
    private HttpCallsServiceManufacturer httpCallsServiceManufacturer;

    public List<Manufacturer> importManufacturers (Resource resources) throws PersistenceException {
        ResourceResolver resourceResolver = resources.getResourceResolver();
        List<Manufacturer> importManufacturers = httpCallsServiceManufacturer.importManufacturerFromApi();

        for (int i=0; i<100; i++) {
            Manufacturer manufacturer = importManufacturers.get(i);
            resourceResolver.create(resources, "manufacturer" + i, new ImmutableMap.Builder<String, Object>()
                    .put("jcr:primaryType", "nt:unstructured")
                    .put("brand", manufacturer.getBrand())
                    .put("id", manufacturer.getId())
                    .build());
            resourceResolver.commit();
        }
        return importManufacturers;
    }
}
