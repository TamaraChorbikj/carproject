package com.carproject.core.services;

import com.carproject.core.models.ImportedUniversity;
import com.carproject.core.models.University;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.osgi.service.component.annotations.Component;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

@Component (service = HttpCallsService.class)
public class HttpCallsService {
    List<University> importUniversityFromApi(){
        ObjectMapper mapper = new ObjectMapper();
        ImportedUniversity importedUniversity = null;
        try {
            URL url = new URL("http://universities.hipolabs.com/search");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();

            if (conn.getResponseCode() == 200) {
                importedUniversity = mapper.readValue(conn.getInputStream(), ImportedUniversity.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return importedUniversity.getResults();
    }
}
