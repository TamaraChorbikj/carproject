package com.carproject.core.services;

import com.carproject.core.interfaces.UniversityDataImportService;
import com.carproject.core.models.University;
import com.google.common.collect.ImmutableMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.List;

@Component(service = UniversityDataImportService.class)
public class UniversityDataImportServiceImpl implements UniversityDataImportService {
    @Reference
    private HttpCallsService httpCallsService;

    public List<University> importUniversity (Resource resources) throws PersistenceException {
        ResourceResolver resourceResolver = resources.getResourceResolver();
        List<University> importUniversity = httpCallsService.importUniversityFromApi();

        return importUniversity;
    }

}
