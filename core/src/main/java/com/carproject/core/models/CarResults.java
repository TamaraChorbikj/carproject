package com.carproject.core.models;

import java.util.ArrayList;
import java.util.List;

public class CarResults {
    private int total;
    private List<CarModel> cars = new ArrayList<>();
    private FilterCars filters;

    public int getTotal() {
        return cars.size();
    }

    public List<CarModel> getCars() {
        return cars;
    }

    public FilterCars getFilters() {
        return filters;
    }

    public void setFilters(FilterCars filters) {
        this.filters = filters;
    }
}

