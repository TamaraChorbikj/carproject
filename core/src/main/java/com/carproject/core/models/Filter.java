package com.carproject.core.models;

import java.util.ArrayList;
import java.util.List;

public class Filter {
    private List<String> name = new ArrayList<>();
    private List<String> country = new ArrayList<>();
    private List<String> stateProvince = new ArrayList<>();
    private List<String> webPage = new ArrayList<>();

    public List<String> getName() {
        return name;
    }

    public List<String> getCountry() {
        return country;
    }

    public List<String> getStateProvince() {
        return stateProvince;
    }

    public List<String> getWebPage() { return webPage; }

    public void setName(List<String> filterName) {
        this.name = filterName;
    }

    public void setCountry(List<String> filterCountry) {
        this.country = filterCountry;
    }

    public void setStateProvince(List<String> filterStateProvince) {
        this.stateProvince = stateProvince;
    }
}

