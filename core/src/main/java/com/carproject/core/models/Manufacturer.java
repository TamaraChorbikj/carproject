package com.carproject.core.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Manufacturer {

    @JsonProperty("Make_ID")
    String id;

    @JsonProperty("Make_Name")
    String brand;

    public String getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
