package com.carproject.core.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ImportedManufacturers {
    @JsonProperty("Results")
    List<Manufacturer> results;

    public List<Manufacturer> getResults() {
        return results;
    }

    public void setResults(List<Manufacturer> results) {
        this.results = results;
    }
}
