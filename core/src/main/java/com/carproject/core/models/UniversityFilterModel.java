package com.carproject.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.ResourcePath;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class)
public class UniversityFilterModel {
    @ResourcePath(path = "/content/carproject/universityData/university")
    private Resource allUniversity;

    private List<UniversityModel> filteredUniversity = new ArrayList<UniversityModel>();

    public UniversityFilterModel() {
    }

    @PostConstruct
    protected void init() {
        allUniversity.getChildren().forEach(this::addUniversityToFilteredList);
    }

    @ValueMapValue(name = "selectedName", injectionStrategy = InjectionStrategy.OPTIONAL)
    @Default(values = "All")
    private String selectedUniversity;

    private void addUniversityToFilteredList(Resource resource) {
        UniversityModel university = resource.adaptTo(UniversityModel.class);

        if ("All".equals(selectedUniversity)) {
            filteredUniversity.add(university);
        }

        if (university.getName().equals(selectedUniversity)) {
            filteredUniversity.add(university);
        }
    }

    public List<UniversityModel> getFilteredUniversity() {
        return filteredUniversity;
    }
}


