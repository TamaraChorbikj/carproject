package com.carproject.core.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ImportedUniversity {
    @JsonProperty("Results")
    List<University> results;

    public List<University> getResults() {
        return results;
    }

    public void setResults(List<University> results) {
        this.results = results;
    }
}
