package com.carproject.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class)
public class UniversityModel {

    @ValueMapValue(injectionStrategy = InjectionStrategy.OPTIONAL)
    private String name;

    @ValueMapValue(injectionStrategy = InjectionStrategy.OPTIONAL)
    private String country;

    @ValueMapValue(injectionStrategy = InjectionStrategy.OPTIONAL)
    private String stateProvince;

    @ValueMapValue(injectionStrategy = InjectionStrategy.OPTIONAL)
    private String webPage;

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public String getWebPage() {
        return webPage;
    }
}