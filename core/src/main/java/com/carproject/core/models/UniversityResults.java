package com.carproject.core.models;

import java.util.ArrayList;
import java.util.List;

public class UniversityResults {
    private int total;
    private List<UniversityModel> university = new ArrayList<>();
    private Filter filters;

    public int getTotal() {
        return university.size();
    }

    public List<UniversityModel> getUniversity() {
        return university;
    }

    public Filter getFilters() {
        return filters;
    }

    public void setFilters(Filter filters) {
        this.filters = filters;
    }
}

