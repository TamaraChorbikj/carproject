package com.carproject.core.models;

import java.util.ArrayList;
import java.util.List;

public class FilterCars {
    private List<String> brands = new ArrayList<>();
    private List<String> model = new ArrayList<>();
    private List<String> year = new ArrayList<>();

    public List<String> getBrands() {
        return brands;
    }

    public List<String> getModel() {
        return model;
    }

    public List<String> getYear() {
        return year;
    }

    public void setBrands(List<String> filterBrands) {
        this.brands = filterBrands;
    }

    public void setModel(List<String> filterModel) {
        this.model = filterModel;
    }

    public void setYear(List<String> filterYear) {
        this.year = year;
    }
}

