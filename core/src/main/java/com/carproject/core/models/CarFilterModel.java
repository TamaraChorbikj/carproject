/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.carproject.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ResourcePath;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.osgi.service.component.annotations.Reference;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class)
public class CarFilterModel {
    @ResourcePath(path = "/content/carproject/carData/cars")
    private Resource allCars;

    private List<CarModel> filteredCars = new ArrayList<CarModel>();

    public CarFilterModel() {
    }

    @PostConstruct
    protected void init() {
        allCars.getChildren().forEach(this::addCarToFilteredList);
    }

    @ValueMapValue(name = "selectedBrand", injectionStrategy = InjectionStrategy.OPTIONAL)
    @Default(values = "All")
    private String selectedCar;

    private void addCarToFilteredList(Resource resource) {
        CarModel car = resource.adaptTo(CarModel.class);

        if ("All".equals(selectedCar)) {
            filteredCars.add(car);
        }

        if (car.getBrand().equals(selectedCar)) {
            filteredCars.add(car);
        }
    }

    public List<CarModel> getFilteredCars() {
        return filteredCars;
    }
}


