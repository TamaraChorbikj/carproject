package com.carproject.core.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class University {

    @JsonProperty("name")
    String name;

    @JsonProperty("country")
    String country;

    @JsonProperty("state-province")
    String stateProvince;

    @JsonProperty("web_pages")
    List<String>
            webPage;

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public String getStateProvince() { return stateProvince; }

    public List<String>
    getWebPage() { return webPage; }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountry(String country) { this.country = country; }

    public void setStateProvince(String stateProvince) { this.stateProvince = stateProvince; }

    public void setWebPage(List<String>
                                   webPage) { this.webPage = webPage; }
}
