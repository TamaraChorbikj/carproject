package com.carproject.core.servlets;

import com.carproject.core.interfaces.UniversityDataService;
import com.carproject.core.models.UniversityResults;
import com.carproject.core.models.Filter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;

@Component(service = {Servlet.class},
        property = {
                Constants.SERVICE_DESCRIPTION + "=University Servlet",
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths=" + "/bin/universitydata"
        }
)

public class UniversityDataServlet extends SlingSafeMethodsServlet {


    private static final Logger LOG = LoggerFactory.getLogger(UniversityDataServlet.class);

    @Reference
    private UniversityDataService UniversityDataService;

    @Override
    protected void doGet(final SlingHttpServletRequest req,
                         final SlingHttpServletResponse resp) throws IOException {

        String name = req.getParameter("name");
        String country = req.getParameter("country");
        String stateProvince = req.getParameter("stateProvince");
        String webPage = req.getParameter("webPage");

        String resourcePath = "/content/carproject/universityData/university";
        ResourceResolver resourceResolver = req.getResourceResolver();
        Resource allUniversity = resourceResolver.getResource(resourcePath);

        UniversityResults results = UniversityDataService.filterUniversity(allUniversity, name, country, stateProvince, webPage);

        Filter filters = UniversityDataService.getDistinctFilters(results.getUniversity());
        results.setFilters(filters);

        ObjectMapper mapper = new ObjectMapper();

        resp.setContentType("application/json");
        resp.getWriter().write(mapper.writeValueAsString(results));
    }

    @Activate
    public void activate() {
        LOG.info("This University DATA SERVLET has been activated!");
    }

    @Deactivate
    public void deactivate() {
        LOG.info("This University DATA SERVLET has been deactivated!");
    }
}
