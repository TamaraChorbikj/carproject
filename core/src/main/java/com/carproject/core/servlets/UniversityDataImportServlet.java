package com.carproject.core.servlets;
import com.carproject.core.interfaces.UniversityDataImportService;
import com.carproject.core.models.University;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import java.io.IOException;
import java.util.List;

@Component(service = {Servlet.class},
        property = {
                Constants.SERVICE_DESCRIPTION + "=University Data Import Servlet",
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths=" + "/bin/importuniversitydata"
        }
)

public class UniversityDataImportServlet extends SlingSafeMethodsServlet {

    @Reference
    private UniversityDataImportService importService;

    @Override
    protected void doGet(final SlingHttpServletRequest req,
                         final SlingHttpServletResponse resp) throws IOException {
        String resourcePath = "/content/carproject/universityData/university";
        ResourceResolver resourceResolver = req.getResourceResolver();
        Resource resources = resourceResolver.getResource(resourcePath);
        ObjectMapper mapper = new ObjectMapper();

        if (resources != null) {
            List<University> University = importService.importUniversity(resources);
            String result = mapper.writeValueAsString(University);
            resp.setContentType("application/json");
            resp.getWriter().write(result);
        }
    }
}

