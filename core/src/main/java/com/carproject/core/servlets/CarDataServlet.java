package com.carproject.core.servlets;

import com.carproject.core.interfaces.CarDataService;
import com.carproject.core.models.CarResults;
import com.carproject.core.models.FilterCars;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import java.io.IOException;

@Component(service = {Servlet.class},
        property = {
                Constants.SERVICE_DESCRIPTION + "=Cars Servlet",
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths=" + "/bin/cardata"
        }
)

public class CarDataServlet extends SlingSafeMethodsServlet {


    private static final Logger LOG = LoggerFactory.getLogger(CarDataServlet.class);

    @Reference
    private CarDataService carDataService;

    @Override
    protected void doGet(final SlingHttpServletRequest req,
                         final SlingHttpServletResponse resp) throws IOException {

        String brand = req.getParameter("brand");
        String model = req.getParameter("model");
        String year = req.getParameter("year");

        String resourcePath = "/content/carproject/carData/cars";
        ResourceResolver resourceResolver = req.getResourceResolver();
        Resource allCars = resourceResolver.getResource(resourcePath);

        CarResults results = carDataService.filterCars(allCars, brand, model, year);

        FilterCars filters = carDataService.getDistinctFilters(results.getCars());
        results.setFilters(filters);

        ObjectMapper mapper = new ObjectMapper();

        resp.setContentType("application/json");
        resp.getWriter().write(mapper.writeValueAsString(results));
    }

    @Activate
    public void activate() {
        LOG.info("This CAR DATA SERVLET has been activated!");
    }

    @Deactivate
    public void deactivate() {
        LOG.info("This CAR DATA SERVLET has been deactivated!");
    }
}
