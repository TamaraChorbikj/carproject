package com.carproject.core.servlets;

import com.day.cq.wcm.api.PageManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;

import javax.jcr.Node;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

@Component(service = { Servlet.class },
        property = {
                Constants.SERVICE_DESCRIPTION + "=Testing Servlet",
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths=" + "/bin/testingAPIs"
        }
)
public class TestingServlet extends SlingSafeMethodsServlet {

    @Override
    protected void doGet(final SlingHttpServletRequest req,
                         final SlingHttpServletResponse resp) throws ServletException, IOException {

        String resourcePath = "/content/carproject";
        ResourceResolver resourceResolver = req.getResourceResolver();

        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);

        try{
            pageManager.create(resourcePath,"test-page", "carproject/components/page", "Test page!");
        } catch (Exception e){
            e.printStackTrace();
        }

        Resource resource = resourceResolver.getResource(resourcePath);

        try{
            resourceResolver.create(resource, "testResource", new ImmutableMap.Builder<String, Object>()
                    .put("jcr:primaryType", "nt:unstructured")
                    .put("title", "THis is a TEST title!")
                    .put("oneProperty", "Property one")
                    .put("anotherProperty", "With a different value")
                    .build());

            resourceResolver.commit();
        } catch (Exception e){
            e.printStackTrace();
        }

        Resource res = resourceResolver.getResource("/content/carproject/testResource");
        Node node = res.adaptTo(Node.class);

        try{
            node.setProperty("title", "I have changed the TITLE");
            node.addNode("childNode");

            resourceResolver.commit();
        } catch (Exception e){
            e.printStackTrace();
        }

        try{
            resourceResolver.delete(res);

            resourceResolver.commit();
        } catch (Exception e){
            e.printStackTrace();
        }


        ObjectMapper mapper = new ObjectMapper();

        resp.setContentType("application/json");
        resp.getWriter().write(mapper.writeValueAsString("OK"));
    }
}
